﻿using System;
using System.Windows.Forms;
using Aquaponics.Core;


namespace Aquaponics.Web.Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSerialize_Click(object sender, EventArgs e)
        {
            SystemStateDto state = new SystemStateDto();
            state.Id = 1;
            state.Name = "SystemState";
            state.SolarTemp = 23;
            state.TankTemp = 22;
            state.AmbientLight = 30;

            txtMessage.Text = Serializers.ToJsonString(state);
        }

        private void btnPost_Click(object sender, EventArgs e)
        {
            try
            {
                txtMessage.Text = GetPostValueObject.HttpPost(edtUri.Text, txtMessage.Text);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}
