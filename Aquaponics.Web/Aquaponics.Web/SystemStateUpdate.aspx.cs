﻿using System;
using System.IO;
using Aquaponics.Core;

namespace Aquaponics.Web
{
    public partial class SystemStateUpdate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int resCode;

            if (Request.HttpMethod == "POST")
            {
                try
                {
                    if (Request.ContentType != null)
                    {
                        //further reading must be done to get the post 
                        StreamReader streamReader = new StreamReader(Request.InputStream, true);
                        string postData = streamReader.ReadToEnd();

                        LogDto log = new LogDto(SystemConstants.LOG_OK, 0, postData, Request.ServerVariables["REMOTE_ADDR"]);
                        SystemStateDto systemState = new SystemStateDto(postData);

                        resCode = SystemConstants.HTTP_OK;

                        ResponseMessageDto responseMessage = new ResponseMessageDto();
                        responseMessage.TestMessage = "Hallo from server";
                        
                        //send response
                        Response.Output.WriteLine(responseMessage.ToJsonString());
                        Response.StatusCode = resCode;
                    }
                }
                catch
                {
                }
            }
            else
            {
                try
                {
                    if (Request.ContentType != null)
                    {
                        //further reading must be done to get the post 
                        StreamReader streamReader = new StreamReader(Request.InputStream, true);
                        string postData = streamReader.ReadToEnd();
                    }
                }
                catch
                {
                }

            }
        }
    }
}