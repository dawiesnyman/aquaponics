﻿namespace Aquaponics.Core
{
    public abstract class Dto
    {
        #region Methods
        public string ToJsonString()
        {
            return Serializers.ToJsonString(this);
        }

        #endregion

        #region Properties
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        #endregion

        #region Fields
        private int id;
        private string name;
        #endregion
    }
}