﻿using System;
using System.Collections.Generic;

namespace Aquaponics.Core.Services
{
    public interface ISystemStateServices
    {
        IServiceResult<IList<SystemStateDto>> GetByDate(DateTime aStartDate, DateTime aEndDate);
        IServiceResult AddSystemState(SystemStateDto state);
        IServiceResult<SystemStateDto> Create();
        IServiceResult Update(SystemStateDto state);
    }
}