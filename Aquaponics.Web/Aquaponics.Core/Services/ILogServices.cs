﻿namespace Aquaponics.Core.Services
{
    public interface ILogServices
    {
        IServiceResult LogMessage();
    }
}
