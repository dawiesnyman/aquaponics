﻿using System.Web.Script.Serialization;

namespace Aquaponics.Core
{
    public class ResponseMessageDto : Dto
    {
        public ResponseMessageDto()
        {
            
        }
        public ResponseMessageDto(string aJsonString)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            ResponseMessageDto tmp = ser.Deserialize<ResponseMessageDto>(aJsonString);

            testMessage = tmp.TestMessage;
        }

        public string TestMessage
        {
            get { return testMessage; }
            set { testMessage = value; }
        }

        #region Fields
        private string testMessage;
        #endregion
    }
}
