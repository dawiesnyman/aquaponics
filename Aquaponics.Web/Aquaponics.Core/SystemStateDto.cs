﻿using System.Web.Script.Serialization;

namespace Aquaponics.Core
{
    public class SystemStateDto :Dto
    {
        public SystemStateDto()
        {}

        public SystemStateDto(string aJsonString)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            SystemStateDto tmp = ser.Deserialize<SystemStateDto>(aJsonString);

            solarTemp = tmp.solarTemp;
            tankTemp = tmp.tankTemp;
            ambientLight = tmp.ambientLight;

        }

        #region Properties
        public float SolarTemp
        {
            get { return solarTemp; }
            set { solarTemp = value; }
        }

        public float TankTemp
        {
            get { return tankTemp; }
            set { tankTemp = value; }
        }

        public float AmbientLight
        {
            get { return ambientLight; }
            set { ambientLight = value; }
        }

        public float ElectricityUsage
        {
            get { return electricityUsage; }
            set { electricityUsage = value; }
        }

        #endregion

        #region Fields
        private float solarTemp;
        private float tankTemp;
        private float ambientLight;
        private float electricityUsage;
        #endregion
    }
}