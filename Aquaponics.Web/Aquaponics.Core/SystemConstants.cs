﻿namespace Aquaponics.Core
{
    public class SystemConstants
    {
        public const int HTTP_OK = 200;
        public const int HTTP_FAIL = 500;

        public const int LOG_OK = 100;
        public const int LOG_FAIL = 101;
        public const int LOG_HTTP_GET_NOT_POST = 102;
        public const int LOG_WARNING = 104;

        public const string HEADER_CHECKSUM = "message_checksum";
    }
}