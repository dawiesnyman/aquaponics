﻿using System.Collections.Generic;

namespace Aquaponics.Core
{
    public interface IServiceResult
    {
        bool IsSuccessfull { get; }
        IList<string> Errors { get; }
    }

    public interface IServiceResult<TObject> :IServiceResult
    {
        IList<TObject> ResultValue { get; }
    }
}