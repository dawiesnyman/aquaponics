﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace Aquaponics.Core
{
    public static class Serializers
    {
        public static string ToXMLString(object aObject)
        {
            XmlSerializer ser = new XmlSerializer(aObject.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);


            ser.Serialize(writer, aObject);

            return sb.ToString();
        }

        ////deserialize
        //JavaScriptSerializer ser = new JavaScriptSerializer();
        //pageSignature pSig = ser.Deserialize<pageSignature>(aJson);

        public static string ToJsonString(object aObject)
        {
            string strJsonLogin = "";
            try
            {
                JavaScriptSerializer ser = new JavaScriptSerializer();

                strJsonLogin = ser.Serialize(aObject).Replace("/", @"\/");  //
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return strJsonLogin;
        }

        //calculate md5 hash
        public static string CalculateMD5Hash(string input)
        {
            //calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);


            //byte[] bytes = System.Text.Encoding.UTF8.GetBytes(aParameters);
            byte[] hash = md5.ComputeHash(inputBytes);

            //convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}