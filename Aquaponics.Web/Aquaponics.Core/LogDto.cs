﻿using System;

namespace Aquaponics.Core
{
    public class LogDto :Dto
    {
        public LogDto()
        {
        }

        public LogDto(int aErrorCode, int aLogTypeName, string aLogData, string aRemoteIp)
        {
            errorCode = aErrorCode;
            logTypeName = aLogTypeName;
            logData = aLogData;
            remoteIp = aRemoteIp;
        }

        #region Properties
        public int ErrorCode
        {
            get { return errorCode; }
            set { errorCode = value; }
        }

        public int LogTypeName
        {
            get { return logTypeName; }
            set { logTypeName = value; }
        }

        public string LogData
        {
            get { return logData; }
            set { logData = value; }
        }

        public string RemoteIp
        {
            get { return remoteIp; }
            set { remoteIp = value; }
        }

        public DateTime EventTime
        {
            get { return eventTime; }
            set { eventTime = value; }
        }
        #endregion

        #region Fields
        private int errorCode;
        private int logTypeName;
        private string logData;
        private string remoteIp;
        private DateTime eventTime;
        #endregion       
    }
}
