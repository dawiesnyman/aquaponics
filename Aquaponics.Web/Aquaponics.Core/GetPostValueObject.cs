﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;

namespace Aquaponics.Core
{
    public class GetPostValueObject
    {
        public static string HttpPost(string aURI, string aParameters)
        {
            string retVal = "";

            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create(aURI);
                //req.Proxy = new System.Net.WebProxy(aURI, true);

                //Add these, as we're doing a POST
                req.ContentType = "application/json";
                req.Method = "POST";

                //We need to count how many bytes we're sending. 
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(aParameters);
                req.ContentLength = bytes.Length;

                System.IO.Stream ostream = req.GetRequestStream();
                ostream.Write(bytes, 0, bytes.Length); //Push it out there
                ostream.Close();

                System.Net.WebResponse resp = req.GetResponse();
                if (resp == null) return null;
                System.IO.StreamReader sr =
                      new System.IO.StreamReader(resp.GetResponseStream());

                retVal = sr.ReadToEnd().Trim();
            }
            catch (Exception ex)
            { throw (ex); }

            return retVal;
        }

        public static string HttpPostWithChecksum(string aURI, string aParameters)
        {
            //string tmp = aParameters.Replace("/", @"\/");

            string retVal = "";
            string Checksum = Serializers.CalculateMD5Hash(aParameters);
            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create(aURI);

                req.Headers.Add(SystemConstants.HEADER_CHECKSUM, Checksum);
                //req.Proxy = new System.Net.WebProxy(aURI, true);

                //Add these, as we're doing a POST
                req.ContentType = "application/json";
                req.Method = "POST";

                //We need to count how many bytes we're sending. 
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(aParameters);
                req.ContentLength = bytes.Length;

                System.IO.Stream ostream = req.GetRequestStream();
                ostream.Write(bytes, 0, bytes.Length); //Push it out there
                ostream.Close();

                System.Net.WebResponse resp = req.GetResponse();

                if (resp == null)
                    return null;

                System.IO.StreamReader sr =
                      new System.IO.StreamReader(resp.GetResponseStream());

                retVal = sr.ReadToEnd().Trim();
            }
            catch (Exception ex)
            { throw (ex); }

            return retVal;
        }

        public static string HttpPostWithChecksumBasicAuth(string aURI, string aParameters, string aUsername, string aPassword)
        {
            //string tmp = aParameters.Replace("/", @"\/");
            NetworkCredential credentials = new NetworkCredential(aUsername, aPassword);
            string retVal = "";
            string Checksum = Serializers.CalculateMD5Hash(aParameters);
            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create(aURI);

                req.Headers.Add(SystemConstants.HEADER_CHECKSUM, Checksum);
                req.Credentials = credentials;

                //req.Proxy = new System.Net.WebProxy(aURI, true);

                //Add these, as we're doing a POST
                req.ContentType = "application/json";
                req.Method = "POST";

                //We need to count how many bytes we're sending. 
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(aParameters);
                req.ContentLength = bytes.Length;

                System.IO.Stream ostream = req.GetRequestStream();
                ostream.Write(bytes, 0, bytes.Length); //Push it out there
                ostream.Close();

                System.Net.WebResponse resp = req.GetResponse();

                if (resp == null)
                    return null;

                System.IO.StreamReader sr =
                      new System.IO.StreamReader(resp.GetResponseStream());

                retVal = sr.ReadToEnd().Trim();
            }
            catch (Exception ex)
            { throw (ex); }

            return retVal;
        }

        public static string HttpGet(string aURI, NameValueCollection aParameters)
        {
            string retVal = "";
            string url = aURI;
            int i = 0;

            //add ? for begin parameters
            if (aParameters.Count > 1)
                url += "?";

            //add all the parameters to the URL
            foreach (string key in aParameters)
            {

                url += key + "=" + aParameters[key];
                i++;

                if (aParameters.Count > 1 && i < aParameters.Count)
                    url += "&";
            }

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(response.GetResponseStream());

                retVal = reader.ReadToEnd();
            }
            catch (Exception ex)
            { throw (ex); }

            return retVal;
        }
    }
}