#ifndef _thermistor_h
#define _thermistor_h
#include <math.h>

const float vcc = 4.91;                       // only used for display purposes, if used set to the measured Vcc.
const float thermr = 10000;                   // thermistor nominal resistance
const float pad = 9850;                     // balance/pad resistor value, set this to the measured resistance of your pad resistor

void PumpOn(int aRelayPin, int aLedPin)
{
    digitalWrite(aRelayPin, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(aLedPin, HIGH);
    //Serial.println("On"); 
}

void PumpOff(int aRelayPin, int aLedPin)
{
	digitalWrite(aRelayPin, LOW);    // turn the LED off by making the voltage LOW
	digitalWrite(aLedPin, LOW); 
	//Serial.println("Off");  
}

float ReadThermistor(int RawADC) 
{
	long Resistance;  
	float Temp;  // Dual-Purpose variable to save space.

	Resistance=((1024 * pad / RawADC) - pad); 
	Temp = log(Resistance); // Saving the Log(resistance) so not to calculate  it 4 times later
	Temp = 1 / (0.001129148 + (0.000234125 * Temp) + (0.0000000876741 * Temp * Temp * Temp));
	Temp = Temp - 273.15;  // Convert Kelvin to Celsius                      

	// Uncomment this line for the function to return Fahrenheit instead.
	//temp = (Temp * 9.0)/ 5.0 + 32.0;                  // Convert to Fahrenheit
	return Temp;                                      // Return the Temperature
}
#endif