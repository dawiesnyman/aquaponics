#ifndef _json_h
#define _json_h


#define TEMP_POST_BUFFER_SIZE 180

String ToJsonString(int solarTemp, int tankTemp)
{
	//char bufPayLoad[TEMP_POST_BUFFER_SIZE];
	//memset (bufPayLoad,'\0', TEMP_POST_BUFFER_SIZE);
	//PString data(bufPayLoad, TEMP_POST_BUFFER_SIZE);
 // 
	//// Build POST expression
 // 
	//data << F("\"SystemState\"") 
	//	<< F(": { ") 
	//	<< F("\"solarTemp\": ") << solarTemp
	//	<< F(", ")
	//	<< F("\"tankTemp\": ") << tankTemp
	//	<< F(" }");

	String data = String("\"SystemState\"");
	data += ": { ";
	data += "\"solarTemp\": ";
	data.concat(solarTemp);
	data += ", ";
	data += "\"tankTemp\": ";
	data.concat(tankTemp);
	data += " }\n";

	return data;
}
#endif