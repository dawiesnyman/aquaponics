/* this is a modifed verrsion of the examples found here:
 *http://playground.arduino.cc/ComponentLib/Thermistor2
 * Schematic:
 *   [Ground] -- [10k-pad-resistor] -- | -- [thermistor] --[Vcc (5 or 3.3v)]
 *                                     |
 *                                Analog Pin 0

 */

#include <math.h>

//-----------------------------------  Pin Setup  ------------------------------------------
//-------------- Analog Pins
#define THERM_ONE 0                 
#define THERM_TWO 1

//-------------- Digital Pins
#define LED_PIN 2
#define RELAY_PIN 3

//---------------------------------- System Constants --------------------------------------
#define MAX_TEMP 30
#define MIN_TEMP 22

const float vcc = 4.91;                       // only used for display purposes, if used set to the measured Vcc.
const float thermr = 10000;                   // thermistor nominal resistance
const float pad = 9850;                     // balance/pad resistor value, set this to the measured resistance of your pad resistor
        
void setup() {
	//Serial.begin(115200);
	pinMode(LED_PIN, OUTPUT);  
	pinMode(RELAY_PIN, OUTPUT);
}

void loop() {
	float tankTemp;
	float solarTemp;
  
	tankTemp = ReadThermistor(analogRead(THERM_ONE));       // read ADC and  convert it to Celsius
	solarTemp = ReadThermistor(analogRead(THERM_TWO)); 
  /*
	Serial.print("One Celsius: "); 
	Serial.print(tankTemp,1);// display Celsius  
	Serial.print("\n");
	Serial.print("Two Celsius: ");
	Serial.print(solarTemp,1);                             // display Celsius

	Serial.println("");                                   
	delay(1000);                                      // Delay a bit... 
*/
	if((tankTemp < MAX_TEMP) && (tankTemp < solarTemp))
	{
		PumpOn(); 
		delay(1000);               // wait for a second
	}
	else
	{
		PumpOff();  
		delay(1000);   
	}
}

void PumpOn()
{
    digitalWrite(RELAY_PIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(LED_PIN, HIGH);
    //Serial.println("On"); 
}

void PumpOff()
{
	digitalWrite(RELAY_PIN, LOW);    // turn the LED off by making the voltage LOW
	digitalWrite(LED_PIN, LOW); 
	//Serial.println("Off");  
}

float ReadThermistor(int RawADC) 
{
	long Resistance;  
	float Temp;  // Dual-Purpose variable to save space.


	Resistance=((1024 * pad / RawADC) - pad); 
	Temp = log(Resistance); // Saving the Log(resistance) so not to calculate  it 4 times later
	Temp = 1 / (0.001129148 + (0.000234125 * Temp) + (0.0000000876741 * Temp * Temp * Temp));
	Temp = Temp - 273.15;  // Convert Kelvin to Celsius                      

	// BEGIN- Remove these lines for the function not to display anything
	//Serial.print("ADC: "); 
	//Serial.print(RawADC); 
	//Serial.print("/1024");                           // Print out RAW ADC Number
	//Serial.print(", vcc: ");
	//Serial.print(vcc,2);
	//Serial.print(", pad: ");
	//Serial.print(pad/1000,3);
	//Serial.print(" Kohms, Volts: "); 
	//Serial.print(((RawADC*vcc)/1024.0),3);   
	//Serial.print(", Resistance: "); 
	//Serial.print(Resistance);
	//Serial.print(" ohms, ");
	// END- Remove these lines for the function not to display anything

	// Uncomment this line for the function to return Fahrenheit instead.
	//temp = (Temp * 9.0)/ 5.0 + 32.0;                  // Convert to Fahrenheit
	return Temp;                                      // Return the Temperature
}

//Json Sample for future use
String ToJsonString(int solarTemp, int tankTemp)
{
  String data = String("\"AquaState\"");
  data += ": { ";
  data += "\"solarTemp\": ";
  data.concat(solarTemp);
  data += ", ";
  data += "\"tankTemp\": ";
  data.concat(tankTemp);
  data += " }";

return data;
}