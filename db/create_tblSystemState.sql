USE [AquaponicsWeb]
GO

/****** Object:  Table [dbo].[tblSystemState]    Script Date: 2013/06/27 02:37:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblSystemState](
	[SystemStateId] [int] NOT NULL,
	[TankSolarTemp] [float] NULL,
	[TankTemp] [float] NULL,
	[Light] [float] NULL,
	[WaterLevel] [float] NULL,
	[TimeStamp] [datetime] NULL
) ON [PRIMARY]

GO


