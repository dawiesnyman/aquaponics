USE [AquaponicsWeb]
GO

/****** Object:  Table [dbo].[tblLog]    Script Date: 2013/06/27 02:36:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ErrorCode] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[LogTypeName] [nvarchar](50) NULL,
	[LogData] [nvarchar](max) NULL,
	[RemoteIP] [nvarchar](14) NULL,
	[EventTime] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


