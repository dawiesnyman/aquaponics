USE [AquaponicsWeb]
GO

/****** Object:  Table [dbo].[tblLogType]    Script Date: 2013/06/27 02:37:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblLogType](
	[LogTypeId] [int] NOT NULL,
	[LogTypeName] [nvarchar](50) NOT NULL
) ON [PRIMARY]

GO


